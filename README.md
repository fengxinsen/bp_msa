# bp_msa

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter development, view the
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

> 注意：在android/app/main下新建assets文件夹，新建supplierconfig.json
```json
{
  "supplier": {
    "vivo": {
      "appid": "100215079"
    },
    "xiaomi": {
    },
    "huawei": {
    },
    "oppo": {
    }
  }
}
```
> appid为对应商店的appid
