package com.baipeng.bp_msa;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import com.bun.miitmdid.core.ErrorCode;
import com.bun.miitmdid.core.MdidSdkHelper;
import com.bun.miitmdid.interfaces.IIdentifierListener;

import java.util.HashMap;
import java.util.Map;

import io.flutter.Log;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/**
 * BpMsaPlugin
 */
public class BpMsaPlugin implements FlutterPlugin, MethodCallHandler {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private MethodChannel channel;

    private Context applicationContext;

    private Handler mainHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            mResult.success(msg.obj);
        }
    };

    private Result mResult;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "bp_msa");
        channel.setMethodCallHandler(this);

        applicationContext = flutterPluginBinding.getApplicationContext();
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);

        channel = null;
        applicationContext = null;
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else if ("getMsa".equals(call.method)) {
            getMsa(result);
        } else {
            result.notImplemented();
        }
    }

    void getMsa(@NonNull Result result) {
        mResult = result;

        final int code = MdidSdkHelper.InitSdk(applicationContext, true, (isSupport, supplier) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("is_supported", supplier.isSupported());
            map.put("oaid", supplier.getOAID());
            map.put("vaid", supplier.getVAID());
            map.put("aaid", supplier.getAAID());
            Log.e("BP_MSA_Map", map.toString());

            Message msg = new Message();
            msg.obj = map;
            mainHandler.sendMessage(msg);
        });
        Log.e("BP_MSA_CODE", code + "");
        switch (code) {
            case ErrorCode.INIT_ERROR_BEGIN:
                mResult.error(String.valueOf(ErrorCode.INIT_ERROR_BEGIN), "INIT_ERROR_BEGIN", null);
                break;
            case ErrorCode.INIT_ERROR_MANUFACTURER_NOSUPPORT:
                mResult.error(String.valueOf(ErrorCode.INIT_ERROR_MANUFACTURER_NOSUPPORT), "不支持的厂商", null);
                break;
            case ErrorCode.INIT_ERROR_DEVICE_NOSUPPORT:
                mResult.error(String.valueOf(ErrorCode.INIT_ERROR_DEVICE_NOSUPPORT), "不支持的设备", null);
                break;
            case ErrorCode.INIT_ERROR_LOAD_CONFIGFILE:
                mResult.error(String.valueOf(ErrorCode.INIT_ERROR_LOAD_CONFIGFILE), "加载配置文件失败", null);
                break;
            case ErrorCode.INIT_ERROR_RESULT_DELAY:
                Log.e("BP_MSA_CODE", "OAID信息将会延迟返回，获取数据可能在异步线程，取决于设备");
                // 信息将会延迟返回，获取数据可能在异步线程，取决于设备
                // mResult.error(String.valueOf(ErrorCode.INIT_ERROR_RESULT_DELAY), "信息将会延迟返回，获取数据可能在异步线程，取决于设备", null);
                break;
            case ErrorCode.INIT_HELPER_CALL_ERROR:
                mResult.error(String.valueOf(ErrorCode.INIT_HELPER_CALL_ERROR), "反射调用失败", null);
                break;
            case 1008616:
                mResult.error(String.valueOf(1008616), "配置文件不匹配", null);
                break;
            default:
                mResult.error(String.valueOf(-1), "INIT_ERROR", null);
                break;
        }
    }
}
