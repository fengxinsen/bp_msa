import 'dart:async';

import 'package:bp_msa/bp_msa_platform_interface.dart';

import 'msa.dart';

class BpMsa {
  Future<String?> getPlatformVersion() {
    return BpMsaPlatform.instance.getPlatformVersion();
  }

  static Future<Msa> msa() async {
    var temp = await BpMsaPlatform.instance.getMsa();
    var msa = Msa.fromJson(temp ?? {});
    return msa;
  }

  Future<Map?> getMsa() async {
    return BpMsaPlatform.instance.getMsa();
  }
}
