import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'bp_msa_method_channel.dart';

abstract class BpMsaPlatform extends PlatformInterface {
  /// Constructs a BpMsaPlatform.
  BpMsaPlatform() : super(token: _token);

  static final Object _token = Object();

  static BpMsaPlatform _instance = MethodChannelBpMsa();

  /// The default instance of [BpMsaPlatform] to use.
  ///
  /// Defaults to [MethodChannelBpMsa].
  static BpMsaPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [BpMsaPlatform] when
  /// they register themselves.
  static set instance(BpMsaPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<Map?> getMsa() {
    throw UnimplementedError('msa() has not been implemented.');
  }
}
