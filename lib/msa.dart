class Msa {
  Msa({
    this.vaid,
    this.isSupported = false,
    this.aaid,
    this.oaid,
  });

  Msa.fromJson(dynamic json) {
    vaid = json['vaid'];
    isSupported = json['is_supported'];
    aaid = json['aaid'];
    oaid = json['oaid'];
  }

  String? vaid;
  bool isSupported = false;
  String? aaid;
  String? oaid;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['vaid'] = vaid;
    map['is_supported'] = isSupported;
    map['aaid'] = aaid;
    map['oaid'] = oaid;
    return map;
  }
}
