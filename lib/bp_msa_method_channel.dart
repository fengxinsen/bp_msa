import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'bp_msa_platform_interface.dart';

/// An implementation of [BpMsaPlatform] that uses method channels.
class MethodChannelBpMsa extends BpMsaPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('bp_msa');

  @override
  Future<String?> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<Map?> getMsa() async {
    final map = await methodChannel.invokeMethod<Map>('getMsa');
    return map;
  }
}
