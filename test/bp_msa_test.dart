import 'package:bp_msa/bp_msa.dart';
import 'package:bp_msa/bp_msa_method_channel.dart';
import 'package:bp_msa/bp_msa_platform_interface.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockBpMsaPlatform
    with MockPlatformInterfaceMixin
    implements BpMsaPlatform {
  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<Map<String, dynamic>?> getMsa() {
    // TODO: implement getMsa
    throw UnimplementedError();
  }
}

void main() {
  final BpMsaPlatform initialPlatform = BpMsaPlatform.instance;

  test('$MethodChannelBpMsa is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelBpMsa>());
  });

  test('getPlatformVersion', () async {
    BpMsa bpMsaPlugin = BpMsa();
    MockBpMsaPlatform fakePlatform = MockBpMsaPlatform();
    BpMsaPlatform.instance = fakePlatform;

    expect(await bpMsaPlugin.getPlatformVersion(), '42');
  });
}
